import java.util.Scanner;
public class XO {
    private static char[][] board = new char[3][3];
    private static char currentPlayer = 'X';

    public static void main(String[] args) {
        initializeBoard();
        System.out.println("Welcome to XO Game!");
        printBoard();

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("It's turn " + currentPlayer + " now!");
            System.out.print("Input (row, col) [1-3]: ");

            int row = 0, col = 0;
            if (scanner.hasNextInt()) {
                row = scanner.nextInt();
            } else {
                scanner.next(); 
                System.out.println("Invalid input, please try again kub.");
                continue;
            }

            if (scanner.hasNextInt()) {
                col = scanner.nextInt();
            } else {
                scanner.next(); 
                System.out.println("Invalid input, please try again kub.");
                continue;
            }
            row -= 1;
            col -= 1;

            if (row < 0 || row >= 3 || col < 0 || col >= 3 || board[row][col] != '-') {
                System.out.println("Invalid input, please try again kub.");
                continue;
            }

            board[row][col] = currentPlayer;
            printBoard();

            if (checkwin()) {
                System.out.println("The winner is " + currentPlayer + "!");
                break;
            }

            if (isBoardFull()) {
                System.out.println("Draw!");
                break;
            }

            currentPlayer = (currentPlayer == 'X') ? 'O' : 'X';
        }

        scanner.close();
    }

    private static void initializeBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }

    private static void printBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static boolean checkwin() {
        for (int i = 0; i < 3; i++) {
            if ((board[i][0] == currentPlayer && board[i][1] == currentPlayer && board[i][2] == currentPlayer) ||
                (board[0][i] == currentPlayer && board[1][i] == currentPlayer && board[2][i] == currentPlayer)) {
                return true;
            }
        }

        if ((board[0][0] == currentPlayer && board[1][1] == currentPlayer && board[2][2] == currentPlayer) ||
            (board[0][2] == currentPlayer && board[1][1] == currentPlayer && board[2][0] == currentPlayer)) {
            return true;
        }

        return false;
    }

    private static boolean isBoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
}
